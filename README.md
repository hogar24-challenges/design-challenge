# Prueba de diseño

# Objetivos

Con esta prueba se busca tener un conocimiento claro de las capacidades del diseñador para la creación de:

* Sitios web atractivos y amigables al usuario.
* Diseño de aplicaciones e interfaces de usuario.
* Conocimiento y entendimiento de los conceptos de material design.

## PARTE 1: Diseño web

### Escenario:

Se desea crear un sitio web de bienes raíces, enfocado a usuarios con edades que oscilan entre los 30 y los 60 años, con familias y en busca de casas o apartamentos 
a la venta. La imagen debe expresar confianza y seguridad.

### Tareas

Por favor crear un boceto de la página principal de dos modos:

1. Orientada a dispositivos móviles de 360 * 640 pixeles.
2. Orientada a pantallas de 1080p

Tiene total libertad creativa en términos de colores y disposición de contenido. No existe logo y no es necesario un logo.

## Parte 2: Interfaces de usuario y material design

### Escenario:
Se cuenta con una aplicación web cuyo nombre es "Administración de propiedades". Dicha aplicación cuenta con la siguiente descripción de las herramientas y menús
visibles de forma permanente:

* Inicio
* Propiedades
    * Centro de control
    * Fotografías
* Anuncios
    * Centro de control
    * Publicaciones
    * Clientes
* Configuración

Adicionalmente están visibles permanentemente los botones de "Usuario" y "Cerrar sesión".

### Tareas
Se desea un diseño que muestre al usuario dicha aplicación haciendo uso de material design https://material.io/design/

# Entregables
Por favor enviar vía correo electrónico las imágenes del diseño realizado. Solo imágenes exportadas en formato png o jpg, bajo ninguna circunstancia
los editables. Igualmente justificar por qué cada diseño y cuál fue el enfoque para su realización.
